import pandas as pd
import pymongo
from pymongo import MongoClient
from pprint import pprint
from bson.objectid import ObjectId
import re

statement_client = MongoClient("mongodb://root:4umgVd9n3M6SHXH3@mongo-replica.ticmas.io:27017")
statement_database = statement_client['learninglocker-colegios']
statement_collection = statement_database['statements']


assets_client = MongoClient("mongodb://root:4umgVd9n3M6SHXH3@mongo-replica.ticmas.io:27017")
assets_database = assets_client['resources-colegios']
assets_collection = assets_database['assets']

groupDist_client = MongoClient("mongodb://root:4umgVd9n3M6SHXH3@mongo-replica.ticmas.io:27017")
groupDist_database = groupDist_client['cdms-colegios']
groupDist_collection = groupDist_database['groupdistributions']


actor_list = []
df = pd.read_csv("C:/Users/Franco Bailon/Desktop/metric-service-instances/Usuarios.csv")
for i in df['USUARIO']:
    i = str(i)
    actor_list.append(i)

dict = []
for actor in actor_list:
    statement_query = {
    'statement.actor.account.name':actor,
    'statement.result.duration':{'$exists': True},
    'statement.result.completion':{'$exists': True}
    }  
    
    objectId = []
    registrationId = []
    stored_list = []
    duration_list = []
    completion_list = []
    
    statement_cursor = statement_collection.find(statement_query)

    for doc in statement_cursor:
        a = re.sub(r'https://', '', doc['statement']['object']['id'], count = 1)
        b = a[0:24]
        objectId.append(b)
        reg = doc['statement']['context']['registration']
        registrationId.append(reg)
        stored = doc['statement']['stored']
        stored_list.append(stored)
        duration = doc['statement']['result']['duration']
        duration_list.append(duration)
        completion = doc['statement']['result']['completion']
        completion_list.append(completion)
        
    assets_name = []
    for i in objectId:
        pipeline = [{
            "$match":{
                "_id": ObjectId(i)
            }
        }]
        assets_cursor = assets_collection.aggregate(pipeline)
        for doc in assets_cursor:
            assets_name.append(doc['title'])
    
    registration_name = []
    for i in registrationId:
        pipeline = [{
            "$match":{
                "registration": i
            }
        }]
        groupDist_cursor = groupDist_collection.aggregate(pipeline)
        for doc in groupDist_cursor:
            registration_name.append(doc['name'])
            
    
    dic = {
        'Registration':registration_name, 
        'Assets': assets_name,
        'Stored':stored_list,
        'Duration': duration_list,
        'Completed':completion_list
    }
    
    df = pd.DataFrame(dic)
    dict.append(df)